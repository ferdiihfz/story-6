from django.shortcuts import render
from .models import Status
import datetime

# Create your views here.
def homepage(request):
    status_many = Status.objects.all().order_by('datetime')
    if request.method == 'POST':
        user_status = request.POST.get('status')
        user_time = datetime.datetime.now()

        status = Status(status=user_status, datetime=user_time)
        status.save()
        return render(request, 'homepage.html', {'statuses' : status_many})

    else:
        return render(request, 'homepage.html', {'statuses' : status_many})